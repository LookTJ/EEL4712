LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY seg7 IS
	PORT (bcd : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
			leds : OUT STD_LOGIC_VECTOR (6 DOWNTO 0));
END seg7;

ARCHITECTURE seg7_behavior OF seg7 IS
BEGIN
	leds <= "1000000" WHEN bcd = "0000" ELSE
			  "1111001" WHEN bcd = "0001" ELSE
			  "0100100" WHEN bcd = "0010" ELSE
			  "0110000" WHEN bcd = "0011" ELSE
			  "0011001" WHEN bcd = "0100" ELSE
			  "0010010" WHEN bcd = "0101" ELSE
			  "0000010" WHEN bcd = "0110" ELSE
			  "1111000" WHEN bcd = "0111" ELSE
			  "0000000" WHEN bcd = "1000" ELSE
			  "0010000" WHEN bcd = "1001" ELSE
			  "-------";
END seg7_behavior;

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY package_sorter IS
	PORT (clk, sort, reset : IN STD_LOGIC;
	weight : IN STD_LOGIC_VECTOR (9 DOWNTO 0);
	grp1, grp2, grp3, grp4 : BUFFER STD_LOGIC_VECTOR (3 DOWNTO 0);
	seg1, seg2, seg3, seg4 : OUT STD_LOGIC_VECTOR (6 DOWNTO 0);
	curr_grp : OUT STD_LOGIC_VECTOR (2 DOWNTO 0));
END package_sorter;

ARCHITECTURE ps_behavior OF package_sorter IS

	SIGNAL l_sort : STD_LOGIC :='1';
	
		COMPONENT seg7
		PORT (bcd : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
				leds : OUT STD_LOGIC_VECTOR (6 DOWNTO 0));
	END COMPONENT;
	
BEGIN
	S7A: seg7 PORT MAP (grp1, seg1);
	S7B: seg7 PORT MAP (grp2, seg2);
	S7C: seg7 PORT MAP (grp3, seg3);
	S7D: seg7 PORT MAP (grp4, seg4);
	
	PROCESS (clk, sort, reset)
	BEGIN
		IF (reset = '0') THEN
			grp1 <= (OTHERS => '0');
			grp2 <= (OTHERS => '0');
			grp3 <= (OTHERS => '0');
			grp4 <= (OTHERS => '0');
			curr_grp <= (OTHERS => '0');
		ELSIF (clk'EVENT AND clk = '1') THEN
			IF (sort = '1' AND l_sort = '0') THEN
				IF (weight > 800) THEN
					grp4 <= 1 + grp4;
					curr_grp <= "100";
				ELSIF (weight > 500) THEN 
					grp3 <= 1 + grp3;
					curr_grp <= "011";
				ELSIF (weight > 200) THEN
					grp2 <= 1 + grp2;
					curr_grp <= "010";
				ELSIF (weight > 0) THEN
					grp1 <= 1 + grp1;
					curr_grp <= "001";
				ELSE curr_grp <= (OTHERS => '0');
				END IF;
			END IF;
			l_sort <= sort;
		END IF;
	END PROCESS;
END ps_behavior;