LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY priority_4to2 IS
	PORT ( w : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
			 y : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
			 z : OUT STD_LOGIC);
END priority_4to2;

ARCHITECTURE Behavior OF priority_4to2 IS
BEGIN
	y <= "11" WHEN w(3) = '1' ELSE
			"10" WHEN w(2) = '1' ELSE
			"01" WHEN W(1) = '1' ELSE
			"00";
	z <= '0' WHEN w = "0000" ELSE '1';
END Behavior;