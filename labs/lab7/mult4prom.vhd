LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY mult4prom IS
	PORT (A4,B4 : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
			P8 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0));
END mult4prom;

ARCHITECTURE Behavior OF mult4prom IS
	SIGNAL addr : STD_LOGIC_VECTOR(7 DOWNTO 0);
BEGIN
ADDR <= A4&B4;
P8 <= X"00" WHEN (A4 = X"0" OR B4 = X"0") ELSE --COVER "0" MULTIPLES
		X"0" & A4 WHEN B4 = X"1" ELSE 		--COVER IDENTIES
		X"0" & B4 WHEN A4 = X"1" ELSE
		"000" & A4 & '0' WHEN B4 = X"2" ELSE -- COVER BIT SHIFT VALUES 2, 4, 8
		"00" & A4 & "00" WHEN B4 = X"4" ELSE
		'0' & A4 & "000" WHEN B4 = X"8" ELSE
		"000" & B4 & '0' WHEN A4 = X"2" ELSE
		"00" & B4 & "00" WHEN A4 = X"4" ELSE
		'0' & B4 & "000" WHEN A4 = X"8" ELSE
		X"09" WHEN (addr = X"33") ELSE --cover multiples
		X"0F" WHEN (addr = X"35" OR addr = X"53") ELSE
		X"12" WHEN (addr = X"36" OR addr = X"63") ELSE
		X"15" WHEN (addr = X"37" OR addr = X"73") ELSE
		X"1B" WHEN (addr = X"39" OR addr = x"93") ELSE
		X"1E" WHEN (addr = X"3A" OR addr = X"A3") ELSE
		X"21" WHEN (addr = X"3B" OR addr = X"B3") ELSE
		X"24" WHEN (addr = X"3C" OR addr = X"C3") ELSE
		X"27" WHEN (addr = X"3D" OR addr = X"D3") ELSE
		X"2A" WHEN (addr = X"3E" OR addr = X"E3") ELSE
		X"2D" WHEN (addr = X"3F" OR addr = X"F3") ELSE
		X"19" WHEN (addr = X"55") ELSE
		X"1E" WHEN (addr = X"56" OR addr = X"65") ELSE
		X"23" WHEN (addr = X"57" OR addr = X"75") ELSE
		X"2D" WHEN (addr = X"59" OR addr = X"95") ELSE
		X"32" WHEN (addr = X"5A" OR addr = X"A5") ELSE
		X"37" WHEN (addr = X"5B" OR addr = X"B5") ELSE
		X"3C" WHEN (addr = X"5C" OR addr = X"C5") ELSE
		X"41" WHEN (addr = X"5D" OR addr = X"D5") ELSE
		X"46" WHEN (addr = X"5E" OR addr = X"E5") ELSE
		X"4B" WHEN (addr = X"5F" OR addr = X"F5") ELSE
		X"24" WHEN (addr = X"66") ELSE
		X"2A" WHEN (addr = X"67" OR addr = X"76") ELSE
		X"36" WHEN (addr = X"69" OR addr = X"96") ELSE
		X"3C" WHEN (addr = X"6A" OR addr = X"A6") ELSE
		X"42" WHEN (addr = X"6B" OR addr = X"B6") ELSE
		X"48" WHEN (addr = X"6C" OR addr = X"C6") ELSE
		X"4E" WHEN (addr = X"6D" OR addr = X"D6") ELSE
		X"54" WHEN (addr = X"6E" OR addr = X"E6") ELSE
		X"5A" WHEN (addr = X"6F" OR addr = X"F6") ELSE
		X"31" WHEN (addr = X"77") ELSE
		X"3F" WHEN (addr = X"79" OR addr = X"97") ELSE
		X"46" WHEN (addr = X"7A" OR addr = X"A7") ELSE
		X"4D" WHEN (addr = X"7B" OR addr = X"B7") ELSE
		X"54" WHEN (addr = X"7C" OR addr = X"C7") ELSE
		X"5B" WHEN (addr = X"7D" OR addr = X"D7") ELSE
		X"62" WHEN (addr = X"7E" OR addr = X"E7") ELSE
		X"69" WHEN (addr = X"7F" OR addr = X"F7") ELSE
		X"51" WHEN (addr = X"99") else
		X"5A" WHEN (addr = X"9A" OR addr = X"A9") ELSE
		X"63" WHEN (addr = X"9B" OR addr = X"B9") ELSE
		X"6C" WHEN (addr = X"9C" OR addr = X"C9") ELSE
		X"75" WHEN (addr = X"9D" OR addr = X"D9") ELSE
		X"7E" WHEN (addr = X"9E" OR addr = X"E9") ELSE
		X"87" WHEN (addr = X"9F" OR addr = X"F9") ELSE
		X"64" WHEN (addr = X"AA") ELSE
		X"6E" WHEN (addr = X"AB" OR addr = X"BA") ELSE
		X"78" WHEN (addr = X"AC" OR addr = X"CA") ELSE
		X"82" WHEN (addr = X"AD" OR addr = X"DA") ELSE
		X"8C" WHEN (addr = X"AE" OR addr = X"EA") ELSE
		X"96" WHEN (addr = X"AF" OR addr = X"FA") ELSE
		X"79" WHEN (addr = X"BB") ELSE
		X"84" WHEN (addr = X"BC" OR addr = X"CB") ELSE
		X"8F" WHEN (addr = X"BD" OR addr = X"DB") ELSE
		X"9A" WHEN (addr = X"BE" OR addr = X"EB") ELSE
		X"A5" WHEN (addr = X"BF" OR addr = X"FB") ELSE
		X"90" WHEN (addr = X"CC") ELSE
		X"9C" WHEN (addr = X"CD" OR addr = X"DC") ELSE
		X"A8" WHEN (addr = X"CE" OR addr = X"EC") ELSE
		X"B4" WHEN (addr = X"CF" OR addr = X"FC") ELSE
		X"A9" WHEN (addr = X"DD") ELSE
		X"B6" WHEN (addr = X"DE" OR addr = X"ED") ELSE
		X"C3" WHEN (addr = X"DF" OR addr = X"FD") ELSE
		X"C4" WHEN (addr = X"EE") ELSE
		X"D2" WHEN (addr = X"EF" OR addr = X"FE") ELSE
		X"E1" WHEN (addr = X"FF") ELSE
		X"00";
END Behavior;