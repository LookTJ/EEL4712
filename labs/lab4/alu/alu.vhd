LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY alu IS
	PORT( A : IN STD_LOGIC_VECTOR (3 DOWNTO 0); --A OPERAND
			B : IN STD_LOGIC_VECTOR (3 DOWNTO 0); --B OPERAND
			Aout, Bout : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
			C : IN STD_LOGIC_VECTOR (2 DOWNTO 0); --OP CODE
			R : BUFFER STD_LOGIC_VECTOR (3 DOWNTO 0); --RESULT
			Z : OUT STD_LOGIC; --ZERO FLAG
			N : OUT STD_LOGIC; --NEGATIVE FLAG
			Cout : OUT STD_LOGIC; --CARRYOUT FLAG
			V : OUT STD_LOGIC); --OVERFLOW FLAG
END alu;

ARCHITECTURE behavior OF alu IS
SIGNAL signs : STD_LOGIC_VECTOR (2 DOWNTO 0);
BEGIN
	PROCESS (A, B, R, C, signs)
	BEGIN
	V <= '0'; Cout  <= '0';
	signs <= A(3) & B(3) & R(3);
	CASE C IS
		WHEN "000" => R <= A AND B;
		WHEN "001" => R <= A OR B;
		WHEN "010" => R <= NOT A;
		WHEN "011" => R <= A+B;
			IF (signs = "110" OR signs = "001")
			THEN V <= '1';
			END IF;
			IF (R < A AND R < B) THEN Cout <= '1';
			END IF;
		WHEN "100" => R <= A+1;
			IF (A = "1111") THEN Cout <= '1';
			END IF;
		WHEN "101" => R <= A-1;
			IF (A = "0000") THEN Cout <= '1';
			END IF;
		WHEN "110" => R <= A-B;
	IF (A < B) THEN Cout <= '1';
	END IF;
	IF (signs = "011" OR signs = "100")
	THEN V <= '1';
	END IF;
	WHEN "111" => R <= 0-A;
	WHEN OTHERS => NULL;
	END CASE;
	N <= R(3);
	Z <= NOT (((R(3) OR R(2)) OR R(1)) OR R(0));
	Aout <= A;
	Bout <= B;
	END PROCESS;
END behavior;