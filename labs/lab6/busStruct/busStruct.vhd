LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY register8 IS
	PORT (
			Load, Clock : IN STD_LOGIC;
			D : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
			Q : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
			);
END register8;

ARCHITECTURE Behavior OF register8 IS
BEGIN
	PROCESS
	BEGIN
		WAIT UNTIL Clock'EVENT AND Clock = '1';
		IF Load ='1' THEN
			Q <= D;
		END IF;
	END PROCESS;
END Behavior;
------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY mux5pr1 IS
	PORT (
			In0, In1, In2, In3, Data : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
			Sel : IN STD_LOGIC_VECTOR (2 DOWNTO 0);
			Z : OUT STD_LOGIC_VECTOR (7 DOWNTO 0)
			);
END mux5pr1;

ARCHITECTURE Behavior OF mux5pr1 IS
BEGIN
	WITH Sel SELECT
		Z <= (In0) WHEN "000",
			  (In1) WHEN "001",
			  (In2) WHEN "010",
			  (In3) WHEN "011",
			  (Data) WHEN OTHERS;
END Behavior;
-----------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY control IS
	PORT (
			ctrlIn : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
			target : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
			muxOut : OUT STD_LOGIC_VECTOR(2 DOWNTO 0)
			);
END control;

ARCHITECTURE Behavior OF control IS
BEGIN
	target <= "0001" WHEN (ctrlIn(3)='0' AND ctrlIn(2)='0') ELSE
				 "0010" WHEN (ctrlIn(3)='0' AND ctrlIn(2)='1') ELSE
				 "0100" WHEN (ctrlIn(3)='1' AND ctrlIn(2)='0') ELSE
				 "1000" WHEN (ctrlIn(3)='1' AND ctrlIn(2)='1') ELSE
				 "----";
	muxOut <= ctrlIn(4)&ctrlIn(1)&ctrlIn(0);
END behavior;
-----------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY busStruct IS
	PORT (
			Clk : IN STD_LOGIC;
			fInput : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
			dInput : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
			busMon : BUFFER STD_LOGIC_VECTOR(7 DOWNTO 0)
			);
END busStruct;

ARCHITECTURE Behavior OF busStruct IS
	COMPONENT mux5pr1
		PORT (
			In0, In1, In2, In3, Data : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
			Sel : IN STD_LOGIC_VECTOR (2 DOWNTO 0);
			Z : OUT STD_LOGIC_VECTOR (7 DOWNTO 0)
			);
	END COMPONENT;
	COMPONENT control
		PORT (
				ctrlIn : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
				target : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
				muxOut : OUT STD_LOGIC_VECTOR(2 DOWNTO 0)
				);
	END COMPONENT;
	COMPONENT register8
		PORT (
			Load, Clock : IN STD_LOGIC;
			D : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
			Q : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
			);
	END COMPONENT;
	SIGNAL muxOut, reg0Out, reg1Out, reg2Out, reg3Out : STD_LOGIC_VECTOR(7 DOWNTO 0);
	SIGNAL muxCtrl : STD_LOGIC_VECTOR(2 DOWNTO 0);
	SIGNAL regCtrl : STD_LOGIC_VECTOR(3 DOWNTO 0);
BEGIN
	BUSMUX: mux5pr1 PORT MAP (reg0Out,reg1Out,reg2Out,reg3Out,dInput,muxCtrl,busMon);
	REG0: register8 PORT MAP (regCtrl(0),Clk,busMon,reg0Out);
	REG1: register8 PORT MAP (regCtrl(1),Clk,busMon,reg1Out);
	REG2: register8 PORT MAP (regCtrl(2),Clk,busMon,reg2Out);
	REG3: register8 PORT MAP (regCtrl(3),Clk,busMon,reg3Out);
	CTRL: control PORT MAP (fInput,regCtrl,muxCtrl);
END Behavior;