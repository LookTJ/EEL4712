LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
USE IEEE.NUMERIC_STD.ALL;

ENTITY comp IS
	PORT ( sum_in : IN INTEGER :=0;
			 pt	  : IN INTEGER :=0;
			 eq	  : OUT STD_LOGIC );
END comp;

ARCHITECTURE Behavior OF comp IS
BEGIN
	PROCESS (sum_in)
	BEGIN
		IF (sum_in = pt) THEN
			eq <= '1';
		ELSE
			eq <= '0';
		END IF;
	END PROCESS;
END Behavior;