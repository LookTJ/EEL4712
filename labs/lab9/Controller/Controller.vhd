LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
USE IEEE.NUMERIC_STD.ALL;

ENTITY Controller IS
	PORT ( Rb, Reset, clk : IN STD_LOGIC;
		    Sum : in STD_LOGIC_VECTOR(3 DOWNTO 0);
			 Roll, Win, Lose : OUT STD_LOGIC);
END Controller;

ARCHITECTURE Behavior OF Controller IS
	SIGNAL State, NextState : INTEGER RANGE 0 TO 5;
	SIGNAL Pt : STD_LOGIC_VECTOR(3 DOWNTO 0);
	SIGNAL Sp : STD_LOGIC;
BEGIN
	PROCESS (Rb, Reset, Sum, State)
	BEGIN
		Sp <= '0';
		Roll <= '0';
		Win <= '0';
		Lose <= '0';
		
		CASE State IS
			WHEN 0 =>
				IF (Rb = '1') THEN
					NextState <= 1;
				ELSE
					NextState <= 0;
				END IF;
			WHEN 1 =>
				IF (Rb = '1') THEN
					Roll <= '1';
					NextState <= 1;
					else
				IF (Sum = "0111" OR Sum = "1011") THEN
					NextState <= 2;
				ELSIF (Sum = "0010" OR Sum = "0011" OR Sum = "1100") THEN
					NextState <= 3; 
				ELSE Sp <= '1';
					NextState <= 4; end if;
				END IF;
			WHEN 2 => Win <= '1';
				IF (Reset = '1') THEN 
					NextState <= 0;
				ELSE NextState <= 2;
				END IF;
			WHEN 3 => Lose <= '1';
				IF (Reset = '1') THEN 
					NextState <= 0;
				ELSE
					NextState <= 3;
				END IF;
			WHEN 4 =>
				IF (Rb = '1') THEN
					NextState <= 5;
				ELSE
					NextState <= 4;
				END IF;
			WHEN 5 => 
				IF (Rb = '1') THEN
					Roll <= '1';
					NextState <= 5;
					else
				IF (Sum = Pt) THEN
					NextState <= 2;
				ELSIF (Sum = "0111") THEN
					NextState <= 3; 
				ELSE NextState <= 4; end if;
				END IF;
		END CASE;
	END PROCESS;
	
	PROCESS (clk)
	BEGIN
		IF (RISING_EDGE(clk)) THEN
			State <= NextState;
			IF Sp = '1' THEN Pt <= Sum;
			END IF;
		END IF;
	END PROCESS;
END Behavior;