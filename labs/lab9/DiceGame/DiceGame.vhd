LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
USE IEEE.NUMERIC_STD.ALL;

ENTITY DiceGame IS
	PORT ( clk	:	IN	STD_LOGIC;
			 Rb_in	:	IN	STD_LOGIC;
			 Reset_in	:	IN	STD_LOGIC;
			 Win_out	:	OUT	STD_LOGIC;
			 Lose_out	:	OUT	STD_LOGIC;
			 seven_seg_sum,seg_7_count1, seg_7_count2  : BUFFER STD_LOGIC_VECTOR(1 TO 7);
			 sum_out, count_out1, count_out2  : BUFFER STD_LOGIC_VECTOR(3 DOWNTO 0));
END DiceGame;

ARCHITECTURE Behavior OF DiceGame IS

COMPONENT Controller
	PORT ( Rb, Reset, clk : IN STD_LOGIC;
		    sum : in STD_LOGIC_VECTOR(3 DOWNTO 0);
			 Roll, Win, Lose : OUT STD_LOGIC);
END COMPONENT;

COMPONENT DiceCntr
	PORT ( 	clk : IN STD_LOGIC;
			 Roll : IN STD_LOGIC;
			 Sum_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
			 count, count2 : BUFFER STD_LOGIC_VECTOR(3 DOWNTO 0));
END COMPONENT;

COMPONENT Seven_seg
	PORT ( num_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
			 leds_0 : OUT STD_LOGIC_VECTOR(1 TO 7));
END COMPONENT;

SIGNAL Roll_out : STD_LOGIC;
--SIGNAL Sum_out : INTEGER RANGE 2 TO 12;

BEGIN

Result : Controller PORT MAP (Rb_in, Reset_in, clk, sum_out, Roll_out, Win_out, Lose_out);
Cntr : DiceCntr PORT MAP (clk, Roll_out, sum_out, Count_out1, Count_out2);
Seven_seg_one : Seven_seg PORT MAP (Count_out1,seg_7_count1);
Seven_seg_two : Seven_seg PORT MAP (Count_out2,seg_7_count2);
seven_seg_three : Seven_seg PORT MAP (sum_out, seven_seg_sum);


--counter2_top : Counter PORT MAP(Roll_out_two, count_out2, Roll_out_three);
--seven_seg_top_one : Seven_seg PORT MAP (count_out,seven_seg_first);
--seven_seg_top_two : Seven_seg PORT MAP (count_out2, seven_seg_second);

END Behavior;