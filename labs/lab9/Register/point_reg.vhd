LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
USE IEEE.NUMERIC_STD.ALL;

ENTITY point_reg IS
	PORT ( sum_in : IN INTEGER :=0;
			 sp_in  : IN STD_LOGIC;
			 reg_out : OUT INTEGER :=0);
END point_reg;

ARCHITECTURE Behavior OF point_reg IS
BEGIN
	PROCESS (sp_in)
	BEGIN
		IF (sp_in = '1') THEN
			reg_out <= sum_in;
		END IF;
	END PROCESS;
END Behavior;