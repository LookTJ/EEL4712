LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
USE IEEE.NUMERIC_STD.ALL;

ENTITY Test_logic IS
	PORT ( clk	:	IN STD_LOGIC;
			 sum_in	: IN INTEGER :=0;
			 D7	:	OUT STD_LOGIC;
			 D711	:	OUT STD_LOGIC;
			 D2312	:	OUT STD_LOGIC );
END Test_logic;

ARCHITECTURE Behavior OF Test_logic IS
BEGIN
	PROCESS (clk)
	BEGIN
	IF (RISING_EDGE(clk)) THEN
		CASE sum_in IS
			WHEN 2 => D2312 <= '1';
			WHEN 3 => D2312 <= '1';
			WHEN 7 =>
				D7 <= '1';
				D711 <= '1';
			WHEN 11 => D711 <= '1';
			WHEN 12 => D2312 <= '1';
			WHEN OTHERS => 
				D7 <= '0';
				D711 <= '0';
				D2312 <= '0';
		END CASE;
	END IF;
	END PROCESS;
END Behavior;