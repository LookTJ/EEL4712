LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
USE IEEE.NUMERIC_STD.ALL;

ENTITY DiceCntr IS
	PORT ( clk : IN STD_LOGIC;
			 Roll : IN STD_LOGIC;
			 Sum_out : OUT INTEGER RANGE 2 TO 12);
END DiceCntr;

ARCHITECTURE Behavior OF DiceCntr IS
	SIGNAL count, count2 : INTEGER range 1 to 6 :=1;

	BEGIN 
		PROCESS(clk)
		BEGIN 
			IF (RISING_EDGE(clk)) THEN
				IF (Roll = '1') THEN
					IF (count = 6) THEN
						count <= 1;
					ELSE
						count <= count + 1;
					END IF;
					IF (count = 6) THEN
						IF (count2 = 6) THEN count2 <= 1;
						ELSE count2 <= count2 + 1;
						END IF;
					END IF;
				END IF;
			END IF;
		END PROCESS;
		Sum_out <= count + count2;
	END Behavior;
	