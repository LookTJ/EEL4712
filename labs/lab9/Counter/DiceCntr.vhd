LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
USE IEEE.NUMERIC_STD.ALL;

ENTITY DiceCntr IS
	PORT ( clk : IN STD_LOGIC;
			 Roll : IN STD_LOGIC;
			 Sum_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
			 count, count2 : BUFFER STD_LOGIC_VECTOR(3 DOWNTO 0));
END DiceCntr;

ARCHITECTURE Behavior OF DiceCntr IS

	BEGIN 
		PROCESS(clk)
		BEGIN 
			IF (RISING_EDGE(clk)) THEN
				IF (Roll = '1') THEN
					IF (count = "0110") THEN
						count <= "0001";
					ELSE
						count <= count + 1;
					END IF;
					IF (count = "0110") THEN
						IF (count2 = "0110") THEN count2 <= "0001";
						ELSE count2 <= count2 + 1;
						END IF;
					END IF;
				END IF;
			END IF;
		END PROCESS;
		Sum_out <= count + count2;
	END Behavior;
	