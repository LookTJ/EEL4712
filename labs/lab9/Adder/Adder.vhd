LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
USE IEEE.NUMERIC_STD.ALL;

ENTITY Adder IS
	PORT ( roll : IN STD_LOGIC;
			 clk : IN STD_LOGIC;
			 counter1 : IN INTEGER :=1;
			 counter2 : IN INTEGER :=1;
			 sum : OUT INTEGER );
END Adder;

ARCHITECTURE Behavior OF Adder IS
BEGIN
	PROCESS
	BEGIN
		WAIT UNTIL clk'event AND clk = '1';
			IF (roll = '1') THEN
				sum <= counter1 + counter2;
			END IF;
	END PROCESS;
END Behavior;