LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
USE IEEE.NUMERIC_STD.ALL;

ENTITY Controller IS
	PORT ( Rb, Reset, clk : IN STD_LOGIC;
			 D7, D711, D2312 : IN STD_LOGIC;
			 Eq	: IN STD_LOGIC;
			 Roll, Win, Lose	: OUT STD_LOGIC;
			 Sp	: OUT STD_LOGIC );
END Controller;

ARCHITECTURE Behavior OF Controller IS
	SIGNAL State, NextState : INTEGER RANGE 0 TO 5;
BEGIN
	PROCESS (Rb, Reset, D7, D711, D2312, Eq)
	BEGIN
		Sp <= '0';
		Roll <= '0';
		Win <= '0';
		Lose <= '0';
		
		CASE State IS
			WHEN 0 =>
				IF (Rb = '1') THEN
					NextState <= 1;
				ELSE
					NextState <= 0;
				END IF;
			WHEN 1 =>
				IF (Rb = '1') THEN
					Roll <= '1';
					NextState <= 1;
				ELSIF (D711 = '1' OR D7 = '1') THEN
					NextState <= 2;
				ELSIF (D2312 = '1') THEN
					NextState <= 3;
				ELSE Sp <= '1';
					NextState <= 4;
				END IF;
			WHEN 2 => Win <= '1';
				IF (Reset = '1') THEN 
					NextState <= 0;
				ELSE NextState <= 2;
				END IF;
			WHEN 3 => Lose <= '1';
				IF (Reset = '1') THEN 
					NextState <= 0;
				ELSE
					NextState <= 3;
				END IF;
			WHEN 4 =>
				IF (Rb = '1') THEN
					NextState <= 5;
				ELSE
					NextState <= 4;
				END IF;
			WHEN 5 => 
				IF (Rb = '1') THEN
					Roll <= '1';
					NextState <= 5;
				ELSIF (Eq = '1') THEN
					NextState <= 2;
				ELSIF (D7 = '1') THEN
					NextState <= 3;
				ELSE NextState <= 4;
				END IF;
		END CASE;
	END PROCESS;
	
	PROCESS (clk)
	BEGIN
		IF (RISING_EDGE(clk)) THEN
			State <= NextState;
		END IF;
	END PROCESS;
END Behavior;